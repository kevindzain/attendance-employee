from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from employee_cbv.models import Employee
# Create your views here.


class EmployeeList(ListView):
    model = Employee

class EmployeeCreate(CreateView):
    model = Employee
    fields = ['no','name', 'position','address','phone','email']
    success_url = reverse_lazy('employee_cbv:employee_list')


class EmployeeUpdate(UpdateView):
    model = Employee
    fields = ['no','name', 'position','address','phone','email']
    success_url = reverse_lazy('employee_cbv:employee_list')

class EmployeeDelete(DeleteView):
    model = Employee
    success_url = reverse_lazy('employee_cbv:employee_list')
