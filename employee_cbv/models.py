
from django.db import models
from django.urls import reverse

# Create your models here.
#class Position(models.Model):

    #def __str__(self):
     #   return self.nameposition

    #nameposition = models.CharField(max_length=100,choices=POSITION_CHS)
   # keterangan = models.TextField(blank=True)

    

 #jenis_kelamin = models.CharField(max_length=10, choices=JENIS_KELAMIN_CHOICES, on_delete=models.CASCADE)

class Employee(models.Model):

    POSITION_CHS = (
        ('Manager', 'Manager'),
        ('Staff Admin', 'Staff Admin'),
        ('Staff Keuangan', 'Staff Keuangan'),
        ('Staff Marketing', 'Staff Marketing'),
    )

    no = models.CharField(max_length=30)
    name = models.CharField(max_length=90)
    position = models.CharField(max_length=30, choices=POSITION_CHS)
    address = models.TextField(max_length=100)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=50)


    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('employee_cbv:employee_edit', kwargs={'pk': self.pk})