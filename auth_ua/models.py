from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save

class USER(models.Model):
    
    username = models.CharField(max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    # created = models.CharField(max_length=30)
    # updated = models.
    
    def __str__(self):
        return self.username

# class Admin(USER):
#     username = models.ForeignKey('TargetModel', related_name='', on_delete=models.CASCADE)
#     phone = models.CharField(max_length=30)

