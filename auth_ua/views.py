from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from auth_ua.models import USER, Admin
# Create your views here.

#USER

class USERList(ListView):
    model = USER

class USERCreate(CreateView):
    model = USER
    fields = ['username','first_name','last_name','password','email']
    success_url = reverse_lazy('auth_ua:auth_ua_list')


class USERUpdate(UpdateView):
    model = USER
    fields = ['username','first_name','last_name','password','email']
    success_url = reverse_lazy('auth_ua:auth_ua_list')

class USERDelete(DeleteView):
    model = USER
    success_url = reverse_lazy('auth_ua:auth_ua_list')

#Admin

class AdminList(ListView):
    model = Admin

class AdminCreate(CreateView):
    model = Admin
    fields = ['username','first_name','last_name','password','email','phone']
    success_url = reverse_lazy('auth_ua:auth_ua_list')


class AdminUpdate(UpdateView):
    model = Admin
    fields = ['username','first_name','last_name','password','email','phone']
    success_url = reverse_lazy('auth_ua:auth_ua_list')

class AdminDelete(DeleteView):
    model = Admin
    success_url = reverse_lazy('auth_ua:auth_ua_list')


