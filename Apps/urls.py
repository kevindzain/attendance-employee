"""Apps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin

from theme import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('employee_cbv/', include('employee_cbv.urls', namespace='employee_cbv')),
    #path('employee_attendance/', include('employee_attendance', namespace='employee_attendance')),
    #path('auth_ua/', include('auth_ua', namespace='auth_ua')),
    #path('holidays/', include('holidays', namespace='holidays')),
    #path('books_fbv_user/', include('books_fbv_user.urls', namespace='books_fbv_user')),
    path('', views.home),
]
