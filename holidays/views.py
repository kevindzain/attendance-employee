# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Import Datetime class
from datetime import datetime

from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils.timezone import datetime
from holidays.models import Holidays
# Create your views here.

class HolidayList(ListView):
    model = Holidays

class HolidayCreate(CreateView):
    model = Holidays
    fields = ['date_f','national_holiday','cuti_bersama']
    success_url = reverse_lazy('holidays:holidays_list')


class HolidayUpdate(UpdateView):
    model = Holidays
    fields = ['date_f','national_holiday','cuti_bersama']
    success_url = reverse_lazy('holidays:holidays_list')

class HolidayDelete(DeleteView):
    model = Holidays
    success_url = reverse_lazy('holidays:holidays_list')

# class HolidayDay():
#     i = Holidays.objects.first()
#     if i.values('date_f_month') + '-' + i.values('date_f_date') in National_Holiday_List:
#         print(National_Holiday_List, "Hari Libur Nasional")

#     elif i.values('date_f_month') + '-' + i.values('date_f_date') in Mass_Leave_List:
#         print(Mass_Leave_List, "Hari Cuti Bersama")
