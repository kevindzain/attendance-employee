from django.db import models
from django.urls import reverse

from django.shortcuts import render

# Import Datetime
from datetime import datetime

# Create your models here.

class Holidays(models.Model):
    # Tahun_Baru_Masehi = '01-01',
    # 02-05 = 'Tahun Baru Imlek'
    # 03-07 = 'Hari Raya Nyepi'
    # 04-03 = 'Isra Miraj'
    # 04-19 = 'Jumat Agung'
    # 05-01 = 'Hari Buruh'
    # 05-19 = 'Hari Raya Waisak'
    # 05-30 = 'Kenaikan Isa Almasih'
    # 06-01 = 'Hari Lahir Pancasila'
    # 06-05 = 'Idul Fitri'
    # 06-06 = 'Idul Fitri'
    # 08-11 = 'Idul Adha'
    # 08-17 = 'Hari Kemerdekaan'
    # 09-01 = 'Tahun Baru Hijriyah'
    # 11-09 = 'Maulid Nabi'
    # 12-25 = 'Hari Natal'

    National_Holiday_List = (
        ('01-01','Tahun Baru Masehi'),
        ('02-05','Tahun Baru Imlek'),
        ('03-07','Hari Raya Nyepi'),
        ('04-03','Isra Miraj'),
        ('04-19','Jumat Agung'),
        ('05-01','Hari Buruh'),
        ('05-19','Hari Raya Waisak'),
        ('05-30','Kenaikan Isa Almasih'),
        ('06-01','Hari Lahir Pancasila'),
        ('06-05','Idul Fitri'),
        ('06-06','Idul Fitri'),
        ('08-11','Idul Adha'),
        ('08-17','Hari Kemerdekaan'),
        ('09-01','Tahun Baru Hijriyah'),
        ('11-09','Maulid Nabi'),
        ('12-25','Hari Natal'),
    )

    Mass_Leave_List = (
        ('06-03','Cuti Bersama 1'),
        ('06-04','Cuti Bersama 1'),
        ('06-07','Cuti Bersama 2'),
        ('12-24','Cuti Bersama 3'),
    )

    year = models.DateField()
    date_f = models.DateField()
    national_holidays = models.CharField(max_length=30,choices=National_Holiday_List)
    cuti_bersama = models.CharField(max_length=30,choices=Mass_Leave_List)


    # def __str__(self):
    #     return self.date_f

    # def _str_(self):
	#     return "{}.{}".format(self.id,self.date_f)

    def get_absolute_url(self):
        return reverse('holidays:holidays_edit', kwargs={'pk': self.pk})

    #     NAMA_KETERANGAN = (
    #     ('Tahun Baru Masehi','Tahun Baru Masehi'),
    #     ('Tahun Baru Imlek','Tahun Baru Imlek'),
    #     ('Hari Raya Nyepi','Hari Raya Nyepi'),
    #     ('Isra Miraj','Isra Miraj'),
    #     ('Jumat Agung','Jumat Agung'),
    #     ('Hari Buruh','Hari Buruh'),
    #     ('Hari Raya Waisak','Hari Raya Waisak'),
    #     ('Kenaikan Isa Almasih','Kenaikan Isa Almasih'),
    #     ('Hari Lahir Pancasila','Hari Lahir Pancasila'),
    #     ('Cuti Bersama 1','Cuti Bersama'),
    #     ('Idul Fitri','Idul Fitri'),
    #     ('Cuti Bersama 2','Cuti Bersama'),
    #     ('Idul Adha','Idul Adha'),
    #     ('Hari Kemerdekaan','Hari Kemerdekaan'),
    #     ('Tahun Baru Hijriyah','Tahun Baru Hijriyah'),
    #     ('Maulid Nabi','Maulid Nabi'),
    #     ('Cuti Bersama 3','Cuti Bersama'),
    #     ('Hari Natal','Hari Natal'),
    # )

    # NH_CHOICES=(
    #     ('1 Januari','1 Januari'),       #Tahun Baru Masehi
    #     ('5 Februari','5 Februari'),     #Tahun Baru Imlek 
    #     ('7 Maret','7 Maret'),           #Hari Raya Nyepi
    #     ('3 April','3 April'),           #Isra Miraj
    #     ('19 April','19 April'),         #Jumat Agung
    #     ('1 Mei','1 Mei'),               #Hari Buruh
    #     ('19 Mei','19 Mei'),             #Hari Raya Waisak
    #     ('30 Mei','30 Mei'),             #Kenaikan Isa Almasih
    #     ('1 Juni','1 Juni'),             #Hari Lahir Pancasila
    #     ('5 Juni','5 Juni'),             #Idul Fitri
    #     ('6 Juni','6 Juni'),             #Idul Fitri
    #     ('11 Agustus','11 Agustus'),     #Idul Adha
    #     ('17 Agustus','17 Agustus'),     #Hari Kemerdekaan
    #     ('1 September','1 September'),   #Tahun Baru Hijriyah
    #     ('9 November','9 November'),     #Maulid Nabi
    #     ('25 Desember','25 Desember'),   #Hari Natal
        
    #     )
    # CB_CHOICES=(
    #     ('3 Juni','3 Juni'),             #Cuti Bersama 1
    #     ('4 Juni','4 Juni'),             #Cuti Bersama 1
    #     ('7 Juni','7 Juni'),             #Cuti Bersama 2
    #     ('24 Desember','24 Desember'),   #Cuti Bersama 3
    #     )
